package za.brinkc.codelist.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import za.brinkc.codelist.controller.CodelistController;
import za.brinkc.codelist.domain.Entry;
import za.brinkc.codelist.service.CodelistService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MockServletContext.class)
@WebAppConfiguration
public class CodelistControllerTest {

	private MockMvc mvc;

	@InjectMocks
	private CodelistController controller;

	@Mock
	CodelistService codelistService;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		mvc = MockMvcBuilders.standaloneSetup(new CodelistController(codelistService))
				.build();
	}

	@Test
	public void codelist() throws Exception {
		when(codelistService.getCodelist(Matchers.matches("country"),
				Matchers.matches("1.0.0"), Matchers.matches("en")))
						.thenReturn(buildgetCodeListResponse());

		mvc.perform(MockMvcRequestBuilders.get("/v1/codelist/country")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	private Map<String, Entry> buildgetCodeListResponse() {
		return new HashMap<>();
	}

	private Entry buildgetCodeResponse() {
		Entry entry = new Entry();
		entry.setCode("ZA");
		entry.setDescription("South Africa");
		entry.setLanguage("en");
		entry.setName("South Africa");
		return entry;
	}

	@Test
	public void code() throws Exception {
		when(codelistService.getCode(Mockito.eq("country"), Mockito.eq("1.0.0"),
				Mockito.eq("za"), Mockito.eq("en"))).thenReturn(buildgetCodeResponse());

		mvc.perform(MockMvcRequestBuilders.get("/v1/codelist/country/za")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().json(
						"{\"code\":\"ZA\",\"language\":\"en\",\"name\":\"South Africa\",\"description\":\"South Africa\"}"));

	}

}
