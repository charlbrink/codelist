package za.brinkc.codelist.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import za.brinkc.codelist.Application;

/**
 * Simple Integration Test
 * 
 * @author charl
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest({ "server.port=0" })
public class CodelistControllerIT {
	@Value("${local.server.port}")
	private int port;

	private URL base;
	private RestTemplate template;

	@Before
	public void setUp() throws Exception {
		this.base = new URL("http://localhost:" + port + "/v1/codelist");
		template = new TestRestTemplate();
	}

	@Test
	public void code() throws Exception {
		ResponseEntity<String> response = template.getForEntity(base.toString()+"/country/za",
				String.class);
		assertThat(response.getBody(), equalTo("{\"code\":\"ZA\",\"language\":\"en\",\"name\":\"South Africa\",\"description\":\"South Africa\"}"));
	}
}
