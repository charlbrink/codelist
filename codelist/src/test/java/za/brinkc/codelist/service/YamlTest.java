package za.brinkc.codelist.service;

import static org.junit.Assert.assertEquals;

import java.beans.IntrospectionException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.yaml.snakeyaml.Yaml;

import za.brinkc.codelist.domain.Codelist;
import za.brinkc.codelist.domain.Entry;

public class YamlTest {

	@Test
	public void testDump() {
		Codelist codelist = buildCodelist();
		Yaml yaml = new Yaml();
		String output = yaml.dump(codelist);
		assertEquals("!!za.brinkc.codelist.domain.Codelist\nentries:\n- {code: za, description: description, language: en, name: name}\n- {code: za, description: description, language: af, name: name}\nidentification: country\nlanguages: [en, af]\nname: country\nversion: 1.0.0\n", output);
	}

	@Test
	public void testCodelistLoad() throws IntrospectionException {
		Codelist codelist = buildCodelist();
		Yaml yaml = new Yaml();
		String output = yaml.dump(codelist);
		Yaml loader = new Yaml();
		Codelist parsed = loader.loadAs(output, Codelist.class);
		assertEquals(codelist.getName(), parsed.getName());
		assertEquals(codelist.getEntries().size(), parsed.getEntries().size());
	}

	private Codelist buildCodelist() {
		Codelist codelist = new Codelist();
		codelist.setIdentification("country");
		codelist.setName("country");
		codelist.setVersion("1.0.0");
		List<String> languages = new ArrayList<>();
		languages.add("en");
		languages.add("af");
		codelist.setLanguages(languages);

		List<Entry> entries = new ArrayList<>();

		Entry entry1 = new Entry();
		entry1.setCode("za");
		entry1.setDescription("description");
		entry1.setLanguage("en");
		entry1.setName("name");
		entries.add(entry1);

		Entry entry2 = new Entry();
		entry2.setCode("za");
		entry2.setDescription("description");
		entry2.setLanguage("af");
		entry2.setName("name");
		entries.add(entry2);
		codelist.setEntries(entries);
		return codelist;
	}

}
