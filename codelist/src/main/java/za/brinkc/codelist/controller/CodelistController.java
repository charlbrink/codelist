/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package za.brinkc.codelist.controller;

import java.util.Collection;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import za.brinkc.codelist.service.CodelistService;

/**
 * REST controller
 * 
 * @author charl
 *
 */
@RestController
@RequestMapping("/v1/codelist")
public class CodelistController {

	public CodelistController() {
		super();
	}

	/**
	 * Required for mocking
	 * @param codelistService
	 */
	public CodelistController(CodelistService codelistService) {
		this.codelistService = codelistService;
	}
	
	@Autowired
	private CodelistService codelistService;
	
	@RequestMapping(value="{codelist}", method = RequestMethod.GET)
	public @ResponseBody Collection<za.brinkc.codelist.domain.Entry> codelist(@PathVariable(value="codelist") String codelist) {
		Map<String, za.brinkc.codelist.domain.Entry> codelistMap = codelistService.getCodelist(codelist, "1.0.0", Locale.getDefault().getLanguage());
		return codelistMap.values();
	}
	
	@RequestMapping(value="{codelist}/{code}", method = RequestMethod.GET)
	public @ResponseBody za.brinkc.codelist.domain.Entry code(@PathVariable(value="codelist") String codelist, @PathVariable(value="code") String code) {
		return codelistService.getCode(codelist, "1.0.0", code, Locale.getDefault().getLanguage());
	}
	
}
