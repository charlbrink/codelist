/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package za.brinkc.codelist;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import za.brinkc.codelist.domain.Codelist;
import za.brinkc.codelist.service.CodelistService;

@SpringBootApplication
@EnableDiscoveryClient
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Configuration
	static class CodelistConfiguration {

		@Bean
		public WebMvcConfigurer corsConfigurer() {
			return new WebMvcConfigurerAdapter() {
				@Override
				public void addCorsMappings(CorsRegistry registry) {
					registry.addMapping("/**");
				}
			};
		}
		
		@Bean
		public CodelistService codelistService() {
			return new CodelistService();
		}

		@Bean
		@Scope(scopeName = "prototype")
		public Yaml yaml() throws ClassNotFoundException {
			Yaml yaml = new Yaml(constructor());
			return yaml;
		}

		@Bean
		@Scope(scopeName = "prototype")
		public Constructor constructor() throws ClassNotFoundException {
			Constructor constructor = new Constructor(Codelist.class);
			return constructor;
		}

	}

	/**
	 * Get list of codelist files to load from config
	 * 
	 * @author charl
	 *
	 */
	@Component
	@ConfigurationProperties
	public static class LoadedCodelists {
		private List<String> codelists = new ArrayList<>();

		public List<String> getCodelists() {
			return codelists;
		}

		public void setCodelists(List<String> codelists) {
			this.codelists = codelists;
		}

	}

}
