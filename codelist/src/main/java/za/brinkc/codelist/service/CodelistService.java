/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package za.brinkc.codelist.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.yaml.snakeyaml.Yaml;

import za.brinkc.codelist.Application.LoadedCodelists;
import za.brinkc.codelist.domain.Codelist;
import za.brinkc.codelist.domain.Entry;

/**
 * 
 * Service for retrieving codelists and codelist entries
 * 
 * @author charl
 *
 */
public class CodelistService {

	Map<String, Map<String, Entry>> codelistMap = new HashMap<>();

	@Autowired
	LoadedCodelists codelists;
	
	@Autowired
	Yaml yaml;
	
	@PostConstruct
	public void init() throws ClassNotFoundException {
		for (String path : codelists.getCodelists()) {
			try {
				loadCodelist(path);
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void loadCodelist(String path) throws IOException, ClassNotFoundException {
		
		try (InputStream in = new ClassPathResource(path).getInputStream()) {
			Codelist codelist = yaml.loadAs(in, Codelist.class);
			for (String language : codelist.getLanguages()) {
				String codelistKey = buildCodelistKey(codelist.getIdentification(), codelist.getVersion(), language);
				Map<String, Entry> codeEntryMap = new HashMap<>();
				codelistMap.put(codelistKey, codeEntryMap);
			}
			for (Entry entry : codelist.getEntries()) {
				Map<String, Entry> codeEntryMap = codelistMap.get(buildCodelistKey(codelist.getIdentification(), codelist.getVersion(), entry.getLanguage()));
				String entryKey = buildEntryKey(entry.getCode());
				codeEntryMap.put(entryKey, entry);
			}
		}
	}

	private String buildCodelistKey(String identification, String version, String language) {
		return identification.toUpperCase()+":"+version.toUpperCase()+":"+language.toUpperCase();		
	}

	private String buildEntryKey(String code) {
		return code.toUpperCase();		
	}
	
	public Map<String, Entry> getCodelist(String identification, String version,
			String language) {
		String codelistKey = buildCodelistKey(identification, version, language);
		if (!codelistMap.isEmpty() && codelistMap.containsKey(codelistKey)) {
			return codelistMap.get(codelistKey);
		}
		return null;
	}

	public Entry getCode(String codelistIdentification, String version, String code,
			String language) {
		String codelistKey = buildCodelistKey(codelistIdentification, version, language);
		if (!codelistMap.isEmpty() && codelistMap.containsKey(codelistKey)) {
			return codelistMap.get(codelistKey).get(buildEntryKey(code));
		}
		return null;
	}

}
